package com.example.android.coffeeorder;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class MainActivity extends Activity {

    // Efetua o bind
    @BindView(R.id.etMainName)
    EditText etMainName;
    @BindView(R.id.ckMainWhippedCream)
    CheckBox ckMainWhippedCream;
    @BindView(R.id.ckMainChocolate)
    CheckBox ckMainChocolate;
    @BindView(R.id.tvMainQuantity)
    TextView tvMainQuantity;
    @BindView(R.id.tvMainValue)
    TextView tvMainValueOrder;
    @BindView(R.id.tvMainOrderSummary)
    TextView tvMainOrderSummary;

    private int quantity = 0;
    private int price = 5;
    private String toppings = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this); // Evocação o ButterKnife para poder efetuar bindings
    }

    @OnClick(R.id.btnMainMinus)
    public void onMinusClicked() {
        if (quantity > 0) {
            quantity -= 1;
            tvMainQuantity.setText("" + quantity);
            // Abaixo apresenta-se a forma de exibir um valor em setText
            //tvMainQuantity.setText(String.valueOf(quantity));
        }
    }

    @OnClick(R.id.btnMainPlus)
    public void onPlusClicked() {
        quantity += 1;
        tvMainQuantity.setText("" + quantity);
    }

    @OnCheckedChanged(R.id.ckMainWhippedCream)
    public void onCheckedWhippedCream(boolean checked) {
        if (checked) {
            toppings += ckMainWhippedCream.getText() + "\n";
        } else {
            toppings += "No Whipped Cream\n";
        }
    }

    @OnCheckedChanged(R.id.ckMainChocolate)
    public void onCheckedChocolate(boolean checked) {
        if (checked) {
            toppings += ckMainChocolate.getText() + "\n";
        } else {
            toppings += "No Chocolate\n";
        }
    }

    @OnClick(R.id.btnMainOrder)
    public void onOrderClicked() {
        tvMainValueOrder.setText(String.valueOf(quantity * price + "€"));

        String name = "" + etMainName.getText();
        String txtOrder = name + "\n" + toppings + "Quantity: " + quantity + "\nTotal Order: " + quantity * price + "€";

        tvMainOrderSummary.setText(txtOrder);

        toppings = "";

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Order for: " + name);
        intent.putExtra(Intent.EXTRA_TEXT, txtOrder);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
